#include<sys/socket.h> // socket,bind,accept,listen,connect
#include<arpa/inet.h> // struct sockaddr_in, htons, inet_addr
#include<stdio.h> 
#include<string.h> //memset
#include<unistd.h> //close

#define PORT_NUM 25519
#define SIZE 256

int main()
{
	int connection_fd = socket(AF_INET,SOCK_STREAM,0);
	if(connection_fd == -1) { perror("Failed to create socket"); return 1;}

	struct sockaddr_in connection_sa;
	memset(&connection_sa,0,sizeof(struct sockaddr_in));
	connection_sa.sin_family = AF_INET;
	connection_sa.sin_port = htons(PORT_NUM);
	connection_sa.sin_addr.s_addr = inet_addr("127.0.0.1");

	connect(connection_fd,(struct sockaddr*)&connection_sa,
			      sizeof(struct sockaddr_in));
	if(connection_fd == -1) { perror("Failed to connect"); return 1;}

	char buff[SIZE] = {0};
	
	printf("Please enter things: ");
	scanf("%s",&buff[0]);

	send(connection_fd,buff,SIZE,0);
       	recv(connection_fd,buff,SIZE,0);
	printf("%s\n",buff);
	close(connection_fd);	

	return 0;
}
