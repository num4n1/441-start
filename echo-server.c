#include<sys/socket.h>
#include<arpa/inet.h>
#include<stdio.h>
#include<stdlib.h> //fork
#include<string.h>
#include<unistd.h>


#define PORT_NUM 25519
#define SIZE 256

//Author: William Black
//emali: william.black@ucalgary.ca

void echo(int);

int main()
{
	printf("Basic application that echos data back using fork() \n");
	

	//Create Socket for incomming TCP connections
	int listener_fd = socket(AF_INET, SOCK_STREAM, 0);
	int connection_fd;

	//Check to see if we failed
	if(listener_fd == -1){ perror("Failed to create socket file discriptor"); return EXIT_FAILURE; }

	//Initialize the socket take to allow connection from any IP on PORT_NUM
	//Note that there are may flags in this magic block and memset make sure 
	//all are off for you, until we turn the ones we want on.
	struct sockaddr_in listener_sa;
	memset(&listener_sa,0,sizeof(struct sockaddr_in));
	listener_sa.sin_family = AF_INET;
	listener_sa.sin_port = htons(PORT_NUM);
	listener_sa.sin_addr.s_addr = htonl(INADDR_ANY);
	//listener_sa.sin_addr = 0;
	
	//Ask the OS really nicely, that the socket to be used on PORT_NUM
	if(bind(listener_fd,(struct sockaddr*) &listener_sa, 
			    sizeof(struct sockaddr_in)) == -1)
	{
		perror("Failed to bind socket\n");
		return EXIT_FAILURE;
	}
	
	//Mark socket as a passive socket i.e one that will accept connections
	//Set the queue length of incoming connections to be 5
	if(listen(listener_fd,5) == -1) { return EXIT_FAILURE; }
	
	//Will never exit this while loop
	//Only when accept errors
	//connection_fd is the fd for the inccoming socket
	while((connection_fd = accept(listener_fd,NULL,NULL)) != -1)
	{
		//This code block is very specifically designed
		//google it if you do not know what it does or why
		int pid = fork();
		if(pid == 0) //Child
		{
			echo(connection_fd);
			close(connection_fd);
			exit(EXIT_SUCCESS);
		}
		else if(pid != 0)//Parrent
		{
			close(connection_fd);
		}
		else//Error
		{
			return EXIT_FAILURE;
		}
	}
	return EXIT_SUCCESS;
}

//Gets data from the client then echos it back
//Then returns form exection.
void echo(int fd)
{
	char buff[SIZE];
	recv(fd,buff,SIZE,0);
	send(fd,buff,SIZE,0);
}
