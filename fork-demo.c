#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>


//What does this do
/*int main()
 {
 	fork();
 	printf("Hello \n");
	return 0;
 }
 */

//How many prints
/*
int main()
{
	for(size_t i= 0; i< 2; i++)
	{
		fork();
		printf("Hello \n");
	}
	return 0;
}
*/


int main()
{
	for(size_t i = 0; i< 100; i++)
	{
		int pid = fork();
		if(pid == 0)// child
		{
			//Do work
			printf("Hello I am a process\n");
			//Kill your self
			exit(1);
		}
		else if( pid > 0)
		{
			//Do nothing;
		}
		else
		{
			//Error?
		}
		
	}
	return 0;
}
